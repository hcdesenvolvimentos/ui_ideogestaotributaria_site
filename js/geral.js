$(function(){

	$('.open-menu').click(function(){
		$('.side-menu-mobile').addClass('open');
	});

	$('.side-menu-mobile span').click(function(){
		$('.side-menu-mobile').removeClass('open');
	});

	var $menu = $('html, body');
	$('.side-menu .menu li a').click(function() {
		$menu.animate({
			scrollTop: $( $.attr(this, 'href') ).offset().top
		}, 500);
		return false;
	});

	var $mobile = $('html, body');
	$('.side-menu-mobile .menu li a').click(function() {
		$mobile.animate({
			scrollTop: $( $.attr(this, 'href') ).offset().top
		}, 500);
		$('.side-menu-mobile').removeClass('open');

		return false;
	});

	var $copy = $('html, body');
	$('footer .copyright .menu-footer li a').click(function() {
		$copy.animate({
			scrollTop: $( $.attr(this, 'href') ).offset().top
		}, 500);

		return false;
	});

});